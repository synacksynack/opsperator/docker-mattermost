#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

generate_salt()
{
    tr -dc a-zA-Z0-9 </dev/urandom | fold -w48 | head -n1
}

urlencode()
{
    curl -s -o /dev/null -w %{url_effective} --get \
	--data-urlencode "$1" "" | cut -c 3-
}

AUTH_METHOD=${AUTH_METHOD:-internal}
DB_DRIVER=${DB_DRIVER:-mysql}
DB_HOST=${DB_HOST:-mysql}
DB_NAME=${DB_NAME:-mattermost}
DB_PASS="${DB_PASS:-secret}"
DB_USER=${DB_USER:-mattermost}
LDAP_SKIP_TLS_VERIFY=true
LLNG_PROTO=${LLNG_PROTO:-http}
OIDC_CLIENT_ID=${OIDC_CLIENT_ID:-mattermost}
OIDC_SECRET=${OIDC_SECRET:-secret}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=matomo,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
OPENLDAP_USERS_OBJECTCLASS=${OPENLDAP_USERS_OBJECTCLASS:-inetOrgPerson}
PROMETHEUS_ENABLE=${PROMETHEUS_ENABLE:-false}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
PUSH_NOTIFS=${PUSH_NOTIFS:-true}
PUSH_HOST=${PUSH_HOST:-https://push-test.mattermost.com}
S3_ACCESS_KEY=${S3_ACCESS_KEY:-}
S3_SECRET_KEY=${S3_SECRET_KEY:-}
S3_BUCKET=${S3_BUCKET:-mattermost}
S3_PATH_PREFIX=${S3_PATH_PREFIX:-}
S3_REGION=${S3_REGION:-us-east-1}
S3_SIG_V2=${S3_SIG_V2:-false}
S3_SSL=${S3_SSL:-true}
SITE_NAME="${SITE_NAME:-KubeChat}"
SMTP_HOST=${SMTP_HOST:-}
SMTP_PORT=${SMTP_PORT:-25}
STORAGE_DRIVER=${STORAGE_DRIVER:-local}
OPENLDAP_SECURITY=
if test -z "$MM_DOMAIN"; then
    MM_DOMAIN=mattermost.$OPENLDAP_DOMAIN
fi
if test -z "$LLNG_SSO_DOMAIN"; then
    LLNG_SSO_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
if test -z "$WS_PROTO"; then
    if test "$PUBLIC_PROTO" = https; then
	WS_PROTO=wss
    else
	WS_PROTO=ws
    fi
fi
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test "$OPENLDAP_HOST"; then
    if test -z "$ADMIN_USER_FILTER"; then
	ADMIN_USER_FILTER="(|(uid=admin0)(memberOf=cn=Admins,ou=groups,$OPENLDAP_BASE))"
    fi
    if test "$OPENLDAP_PROTO" = ldaps; then
	OPENLDAP_SECURITY=TLS
    fi
fi
if test "$AUTH_METHOD" = lemon -o "$AUTH_METHOD" = oidc; then
    OIDC_ENABLE=true
    OPENLDAP_ENABLE=true
    if test "$OIDC_LOCKOUT"; then
	SIGNUP_ENABLE=false
    else
	SIGNUP_ENABLE=true
    fi
elif test "$AUTH_METHOD" = ldap; then
    OIDC_ENABLE=false
    OPENLDAP_ENABLE=true
    SIGNUP_ENABLE=true
else
    OIDC_ENABLE=false
    OPENLDAP_ENABLE=false
    SIGNUP_ENABLE=true
fi
if test -z "$S3_ENDPOINT"; then
    S3_ENDPOINT=s3.amazonaws.com
fi
if test -z "$SMTP_NOREPLY"; then
    SMTP_NOREPLY=noreply@$OPENLDAP_DOMAIN
fi
if test "$SMTP_HOST"; then
    SMTP_NOTIFS=true
else
    SMTP_NOTIFS=false
fi
. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

cpt=0
if test "$DB_DRIVER" = postgres; then
    DB_PORT=${DB_PORT:-5432}
    echo Waiting for Postgres backend ...
    while true
    do
	if echo '\d' | PGPASSWORD="$DB_PASS" \
		psql -U "$DB_USER" -h "$DB_HOST" \
		-p "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " Postgres is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach Postgres" >&2
	    exit 1
	fi
	sleep 5
	echo Postgres is ... KO
	cpt=`expr $cpt + 1`
    done
else
    DB_PORT=${DB_PORT:-3306}
    echo Waiting for MySQL backend ...
    while true
    do
	if echo SHOW TABLES | mysql -u "$DB_USER" \
		--password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME" >/dev/null 2>&1; then
	    echo " MySQL is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach MySQL" >&2
	    exit 1
	fi
	sleep 5
	echo MySQL is ... KO
	cpt=`expr $cpt + 1`
    done
fi
if test "$S3_ACCESS_KEY" -a "$S3_SECRET_KEY" -a "$S3_ENDPOINT" -a \
	-x /usr/bin/s4cmd; then
    cpt=0
    if echo "$S3_ENDPOINT" | grep : >/dev/null; then
	S3_HOST=`echo $S3_ADDR | cut -d: -f1`
	S3_PORT=`echo $S3_ADDR | cut -d: -f2`
    else
	if test "$S3_SSL" = true; then
	    S3_PORT=443
	else
	    S3_PORT=80
	fi
	S3_HOST=$S3_ADDR
    fi
    echo Waiting for s3 backend ...
    sed -e "s|S3_ACCESS_KEY|$S3_ACCESS_KEY|g" \
	-e "s|S3_HOST|$S3_HOST|g" -e "s|S3_PORT|$S3_PORT|g" \
	-e "s|S3_SECRET_KEY|$S3_SECRET_KEY|g" \
	-e "s|S3_SIG_V2|$S3_SIG_V2|g" \
	-e "s|S3_SSL|$S3_SSL|g" \
	/s3cfg >/tmp/.s3cfg
    while true
    do
	if s4cmd -c /tmp/.s3cfg ls; then
	    echo " s3 is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo "Could not reach s3" >&2
	    exit 1
	fi
	sleep 5
	echo s3 is ... KO
	cpt=`expr $cpt + 1`
    done
    rm -f /tmp/.s3cfg
fi
if test "$OPENLDAP_HOST"; then
    echo Waiting for LDAP backend ...
    cpt=0
    while true
    do
	if LDAPTLS_REQCERT=never \
		ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    echo " LDAP is alive!"
	    break
	elif test "$cpt" -gt 25; then
	    echo Could not reach OpenLDAP >&2
	    exit 1
	fi
	sleep 5
	echo LDAP is ... KO
	cpt=`expr $cpt + 1`
    done
    if test "$OPENLDAP_PROTO" = ldaps; then
	if ldapsearch -H $OPENLDAP_PROTO://$OPENLDAP_HOST:$OPENLDAP_PORT \
		-D "$OPENLDAP_BIND_DN_PREFIX,$OPENLDAP_BASE" \
		-b "ou=users,$OPENLDAP_BASE" \
		-w "$OPENLDAP_BIND_PW" \
		uid >/dev/null 2>&1; then
	    LDAP_SKIP_TLS_VERIFY=false
	fi
    fi
fi

genConf()
{
    echo "{"
    DB_PASS=$(urlencode "$DB_PASS")
    DB_USER=$(urlencode "$DB_USER")
    sed -e "s|SITE_NAME|$SITE_NAME|" \
	-e "s|MM_DOMAIN|$MM_DOMAIN|" \
	-e "s|PUBLIC_PROTO|$PUBLIC_PROTO|" \
	-e "s|WS_PROTO|$WS_PROTO|" \
	/config.d/00-main.json
    sed -e "s|DB_DRIVER|$DB_DRIVER|" \
	-e "s|DB_NAME|$DB_NAME|" \
	-e "s|DB_HOST|$DB_HOST|" \
	-e "s|DB_PASS|$DB_PASS|" \
	-e "s|DB_PORT|$DB_PORT|" \
	-e "s|DB_USER|$DB_USER|" \
	/config.d/01-db.json
    sed -e "s|S3_ACCESS_KEY|$S3_ACCESS_KEY|" \
	-e "s|S3_SECRET_KEY|$S3_SECRET_KEY|" \
	-e "s|S3_BUCKET|$S3_BUCKET|" \
	-e "s|S3_PATH_PREFIX|$S3_PATH_PREFIX|" \
	-e "s|S3_REGION|$S3_REGION|" \
	-e "s|S3_ENDPOINT|$S3_ENDPOINT|" \
	-e "s|S3_SSL|$S3_SSL|" \
	-e "s|S3_SIG_V2|$S3_SIG_V2|" \
	-e "s|FILES_SALT|$FILES_SALT|" \
	-e "s|STORAGE_DRIVER|$STORAGE_DRIVER|" \
	/config.d/02-files.json
    sed -e "s|SIGNUP_ENABLE|$SIGNUP_ENABLE|" \
	-e "s|SMTP_HOST|$SMTP_HOST|" \
	-e "s|SMTP_NOTIFS|$SMTP_NOTIFS|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	-e "s|PUSH_NOTIFS|$PUSH_NOTIFS|" \
	-e "s|PUSH_HOST|$PUSH_HOST|" \
	-e "s|SMTP_NOREPLY|$SMTP_NOREPLY|" \
	/config.d/03-smtp.json
    sed -e "s~ADMIN_USER_FILTER~$ADMIN_USER_FILTER~" \
	-e "s|LLNG_PROTO|$LLNG_PROTO|" \
	-e "s|LLNG_SSO_DOMAIN|$LLNG_SSO_DOMAIN|" \
	-e "s|OIDC_CLIENT_ID|$OIDC_CLIENT_ID|" \
	-e "s|OIDC_ENABLE|$OIDC_ENABLE|" \
	-e "s|OIDC_SECRET|$OIDC_SECRET|" \
	-e "s|LDAP_SKIP_TLS_VERIFY|$LDAP_SKIP_TLS_VERIFY|" \
	-e "s|OPENLDAP_BASE|$OPENLDAP_BASE|" \
	-e "s|OPENLDAP_BIND_DN_PREFIX|$OPENLDAP_BIND_DN_PREFIX|" \
	-e "s|OPENLDAP_BIND_PW|$OPENLDAP_BIND_PW|" \
	-e "s|OPENLDAP_DOMAIN|$OPENLDAP_DOMAIN|" \
	-e "s|OPENLDAP_ENABLE|$OPENLDAP_ENABLE|" \
	-e "s|OPENLDAP_HOST|$OPENLDAP_HOST|" \
	-e "s|OPENLDAP_PORT|$OPENLDAP_PORT|" \
	-e "s|OPENLDAP_PROTO|$OPENLDAP_PROTO|" \
	-e "s|OPENLDAP_USERS_OBJECTCLASS|$OPENLDAP_USERS_OBJECTCLASS|" \
	-e "s|OPENLDAP_SECURITY|$OPENLDAP_SECURITY|" \
	/config.d/04-auth.json
    cat /config.d/05-es.json
    cat /config.d/06-cluster.json
    sed -e "s|PROMETHEUS_ENABLE|$PROMETHEUS_ENABLE|" \
	/config.d/07-monit.json
    cat /config.d/08-branding.json
    echo "}"
}

if ! test -s /mattermost/config/config.json; then
    echo === Generates Site Configuration ===
    if test -z "$FILES_SALT"; then
	FILES_SALT=`generate_salt`
    fi
    genConf >/mattermost/config/config.json
elif test "$RESET_CONF" = true; then
    echo === Resetting Site Configuration ===
    FILES_SALT=$(awk '/PublicLinkSalt/{print $2}' /mattermost/config/config.json | sed 's|[",]||g')
    genConf >/mattermost/config/config.json
else
    echo === Re-using Previous Configuration ===
fi

if test "$DEBUG_CONF"; then
    echo === Using Configuration ===
    cat /mattermost/config/config.json
    echo ===
fi

echo === Executing provision.sh ===
/usr/local/bin/provision.sh &

echo === Starting Mattermost ===
exec /mattermost/bin/mattermost -cconfig.json
