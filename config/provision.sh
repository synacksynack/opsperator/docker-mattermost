#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

LANG=${LANG:-en}
MM_ADMIN_PASSWORD=${MM_ADMIN_PASSWORD:-MySecret-42}
MM_ADMIN_USERNAME=${MM_ADMIN_USERNAME:-mmadm}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
SITE_NAME="${SITE_NAME:-KubeChat}"

echo NOTICE: Waiting for MatterMost to startup
while :
do
    if curl --connect-timeout 4 http://127.0.0.1:8080 \
	    -w %{http_code} -o /dev/null 2>/dev/null \
	    | grep 200 >/dev/null; then
	break
    fi
    sleep 10
    echo ....... Waiting for MatterMost to startup
done

LNAME=`echo "$SITE_NAME" | tr A-Z a-z | sed 's|[^a-z]||g'`
if ! /mattermost/bin/mattermost user search "$MM_ADMIN_USERNAME" \
	2>/dev/null | grep -E "^username: $MM_ADMIN_USERNAME$" \
	>/dev/null; then
    echo "NOTICE: Creating $MM_ADMIN_USERNAME User"
    if ! /mattermost/bin/mattermost user create \
	    --email "$MM_ADMIN_USERNAME@$OPENLDAP_DOMAIN" \
	    --firstname "$MM_ADMIN_USERNAME" \
	    --username "$MM_ADMIN_USERNAME" \
	    --password "$MM_ADMIN_PASSWORD" \
	    --locale "$LANG" \
	    --system_admin 2>&1; then
	echo "CRITICAL: Failed creating $MM_ADMIN_USERNAME account"
    fi
else
    echo NOTICE: User $MM_ADMIN_USERNAME already exists
fi
if ! /mattermost/bin/mattermost team search "$LNAME" \
	2>/dev/null | grep -E "^$LNAME: " >/dev/null; then
    echo "NOTICE: Creating $SITE_NAME Team"
    if ! /mattermost/bin/mattermost team create \
	    --name "$LNAME" --display_name "$SITE_NAME"; then
	echo CRITICAL: Failed creating initial team
    elif ! /mattermost/bin/mattermost team add \
	    "$LNAME" "$MM_ADMIN_USERNAME"; then
	echo CRITICAL: Failed adding initial user to team
    elif ! /mattermost/bin/mattermost team modify \
	    "$LNAME" --public; then
	echo "CRITICAL: Failed making $SITE_NAME public"
    else
	echo NOTICE: Done setting up initial Team and Channel
    fi
fi

if test -s /mattermost/sub/license.mattermost-license; then
    if ! /mattermost/bin/mattermost license upload \
	    /mattermost/sub/license.mattermost-license; then
	echo WARNING: Failed uploading license
    else
	echo NOTICE: Successfully uploaded license
    fi
fi
