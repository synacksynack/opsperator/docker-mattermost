#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/mattermost-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to mattermost
	if grep ^mattermost: /etc/group >/dev/null; then
	    sed "s|^mattermost:.*|mattermost:x:`id -g`:|" /etc/group
	else
	    cat /etc/group
	    echo mattermost:x:`id -g`:
	fi >/tmp/mattermost-group
	if grep ^mattermost: /etc/passwd >/dev/null; then
	    sed \
		"s|^mattermost:.*|mattermost:x:`id -u`:`id -g`:mattermost:/mattermost:/bin/sh|" \
		/etc/passwd
	else
	    echo mattermost:x:`id -u`:`id -g`::/mattermost:/bin/sh
	fi >/tmp/mattermost-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/mattermost-passwd
    export NSS_WRAPPER_GROUP=/tmp/mattermost-group
    if test -s /usr/lib64/libnss_wrapper.so; then
	export LD_PRELOAD=/usr/lib64/libnss_wrapper.so
    else
	export LD_PRELOAD=/usr/lib/libnss_wrapper.so
    fi
fi
