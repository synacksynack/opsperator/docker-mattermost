should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if test -d /etc/pki/ca-trust/source/anchors; then
	    dir=/etc/pki/ca-trust/source/anchors
	else
	    dir=/usr/local/share/ca-certificates
	fi
	d=`echo $f | sed 's|/|-|g'`
	if ! test -s $dir/kube$d.crt; then
	    if ! cat $f >$dir/kube$d; then
		echo WARNING: failed installing $f certificate authority >&2
	    else
		should_rehash=true
	    fi
	fi
    fi
done
if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash
