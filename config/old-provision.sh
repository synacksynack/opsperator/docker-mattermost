#!/bin/sh
# sample below does not work (yet)
# users can not login
# tried to force its other fields (DeleteAt and ...?) matching other
# accounts...
# mattermost binary currently allows us to create these, though it
# can require 10-20 minutes for 5 commands to run .... (?!).
# Note matttermost should be replaced with mmctl, as the former is
# being deprecated. Though the latter uses API, and assumes you already
# have an administrative account active. Thus, mmctl can not be used
# bootstraping/provisioning a new deployment. Hence: that shit below
# better work sooner than later ...
# note: apache2-utils package removed from dockerfile, required
# hashing user pw

if test "$DEBUG"; then
    set -x
fi

MM_ADMIN_PASSWORD=MySecret-42
MM_ADMIN_USERNAME=mmadm
DB_DRIVER=${DB_DRIVER:-mysql}
DB_HOST=${DB_HOST:-mysql}
DB_NAME=${DB_NAME:-mattermost}
DB_PASS="${DB_PASS:-secret}"
DB_USER=${DB_USER:-mattermost}
LANG=${LANG:-en}
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
SITE_NAME="${SITE_NAME:-KubeChat}"
WHEN=$(date +%s)000

if test "$DB_DRIVER" = postgres; then
    DB_PORT=${DB_PORT:-5432}
    echo TODO
else
    DB_PORT=${DB_PORT:-3306}
    USERID=$(echo "SELECT Id FROM Users WHERE Username = '$MM_ADMIN_USERNAME'" \
		| mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME" 2>/dev/null)
    if test "$USERID"; then
	echo NOTICE: Admin user already exists
    else
	USERID=`cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 26 | head -n 1`
	HASHED_PW=`htpasswd -bnBC 10 "" "$MM_ADMIN_PASSWORD" | tr -d ':\n' | sed 's/$2y/$2a/'`
	if ! ( echo "INSERT INTO Users (Id,Username,Password,AuthService,Email,"
		echo " EmailVerified,Nickname,FirstName,LastName,Position,Roles,"
		echo " AllowMarketing,Props,Locale,MfaActive,MfaSecret,CreateAt,UpdateAt)"
		echo " VALUES ('$USERID','$MM_ADMIN_USERNAME','$HASHED_PW','',"
		echo " '$MM_ADMIN_USERNAME@$OPENLDAP_DOMAIN',0,'$MM_ADMIN_USERNAME','','','', "
		echo " 'system_admin system_user',0,'{}','$LANG',0,'',$WHEN,$WHEN)"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed inserting initial admin user
	elif ! ( echo "INSERT INTO Preferences (UserId, Category, Name, Value)"
		echo " VALUES ('$USERID','tutorial_step','$USERID','0')"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed disabling tutorial for admin user
	fi
    fi
    if echo "SELECT Id, Name FROM Teams WHERE Name = '$SITE_NAME'" \
	    | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
	    -P "$DB_PORT" "$DB_NAME" 2>/dev/null \
	    | grep "$SITE_NAME" >/dev/null; then
	echo NOTICE: Initial Team already exists
    else
	CHANID=`cat /dev/urandom | tr -dc a-z0-9 | fold -w 26 | head -n 1`
	TEAMID=`cat /dev/urandom | tr -dc a-z0-9 | fold -w 26 | head -n 1`
	INVITEID=`cat /dev/urandom | tr -dc a-z0-9 | fold -w 26 | head -n 1`
	LNAME=`echo "$SITE_NAME" | tr A-Z a-z`
	if ! ( echo "INSERT INTO Teams (Id,DisplayName,Name,Description,Email,Type,"
	       echo " CompanyName,AllowedDomains,InviteId,AllowOpenInvite)"
	       echo " VALUES ('$TEAMID','$SITE_NAME','$LNAME','$SITE_NAME',"
	       echo " '$MM_ADMIN_USER@$OPENLDAP_DOMAIN','O','$SITE_NAME','',"
	       echo " '$INVITEID',0)"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed inserting initial Team
	elif ! ( echo "INSERT INTO TeamMembers (TeamId,UserId,Roles,SchemeUser,SchemeAdmin,SchemeGuest)"
	         echo " VALUES ('$TEAMID','$USERID','',1,1,0)"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed inserting admin user as a member of initial Team
	elif ! ( echo "INSERT INTO Channels (Id,TeamId,Type,DisplayName,Name,"
	         echo " Header,Purpose,TotalMsgCount,ExtraUpdateAt,CreatorId)"
	         echo " VALUES ('$CHANID','$TEAMID','O','General','general',"
		 echo " '','',0,0,'')"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed inserting initial Channel in initial Team
	elif ! ( echo "INSERT INTO PublicChannels (Id,TeamId,DisplayName,Name,Header,Purpose)"
	         echo " VALUES ('$CHANID','$TEAMID','General','general','','')"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed marking initial Channel in initial Team as Public
	elif ! ( echo "INSERT INTO ChannelMembers (ChannelId,UserId,Roles,SchemeUser,SchemeAdmin,SchemeGuest)"
	         echo " VALUES ('$CHANID','$USERID','',1,1,0)"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed subscribing admin user to initial Channel in initial Team
	elif ! ( echo "INSERT INTO ChannelMemberHistory (ChannelId,UserId,JoinTime)"
	         echo " VALUES ('$CHANID','$USERID',$WHEN)"
	    ) | mysql -u "$DB_USER" --password="$DB_PASS" -h "$DB_HOST" \
		-P "$DB_PORT" "$DB_NAME"; then
	    echo WARNING: Failed updating admin user channels membership history
	else
	    echo NOTICE: Done setting up initial Team and Channel
	fi
    fi
fi
