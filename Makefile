SKIP_SQUASH?=1
MYIP=10.42.42.127
IMAGE=opsperator/mattermost
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: buildoc
buildoc:
	SETUP_OC=true SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	SKIP_SQUASH=$(SKIP_SQUASH) TAG_ON_SUCCESS=$(TAG_ON_SUCCESS) \
	    TEST_MODE=true hack/build.sh

.PHONY: run
run:
	@@docker run -e MATTERMOST_DOMAIN=mattermost.demo.local \
	    -p 8080:8080 $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in mariadb-secret mariadb-service mariadb-deployment \
		secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "MATTERMOST_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "MATTERMOST_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ha.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemopersistent
ocdemopersistent: ocbuild
	@@if ! oc describe secret openldap-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml -p FRONTNAME=$(FRONTNAME) \
	    | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
