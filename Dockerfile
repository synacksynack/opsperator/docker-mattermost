FROM docker.io/debian:buster-slim

# Mattermost image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    MM_INSTALL_TYPE=docker \
    MM_REL=https://releases.mattermost.com \
    MM_VERSION=6.0.2 \
    MMARM_REL=https://github.com/SmartHoneybee/ubiquitous-memory/releases/download \
    PATH=/mattermost/bin:${PATH}

LABEL io.k8s.description="Mattermost Image." \
      io.k8s.display-name="Mattermost $MM_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="mattermost,chat" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mattermost" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MM_VERSION"

COPY config /

RUN set -x \
    && mv /provision.sh /reset-tls.sh /nsswrapper.sh /usr/local/bin/ \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install Mattermost Dependencies" \
    && apt-get -y install ca-certificates curl libffi-dev tzdata libcap2 s4cmd \
	mariadb-client postgresql-client ldap-utils openssl libnss-wrapper \
    && echo "# Install Mattermost" \
    && if test "$DO_ENTERPRISE"; then \
	if test `uname -m` = aarch64; then \
	    echo FIXME: unsupported architecture; \
	    exit 1; \
	elif test `uname -m` = armv7l; then \
	    echo FIXME: unsupported architecture; \
	    exit 1; \
	fi \
	&& curl "$MM_REL/$MM_VERSION/mattermost-$MM_VERSION-linux-amd64.tar.gz" \
	    | tar -xz -C /; \
    elif test `uname -m` = aarch64; then \
	curl -fsL "$MMARM_REL/v$MM_VERSION/mattermost-v$MM_VERSION-linux-arm64.tar.gz" \
	    | tar -xz -C /; \
    elif test `uname -m` = armv7l; then \
	curl -fsL "$MMARM_REL/v$MM_VERSION/mattermost-v$MM_VERSION-linux-arm.tar.gz" \
	    | tar -xz -C /; \
    else \
	curl "$MM_REL/$MM_VERSION/mattermost-team-$MM_VERSION-linux-amd64.tar.gz" \
	    | tar -xz -C /; \
    fi \
    && mv /mattermost/config/config.json /mattermost/config/config.json.sample \
    && mkdir -p /mattermost/data /mattermost/plugins \
	/mattermost/client/plugins \
    && for i in /mattermost /etc/ssl/certs /usr/local/share/ca-certificates; \
	do \
	    chown -R 1001:root "$i" \
	    && chmod -R g=u "$i"; \
	done \
    && echo "# Cleaning Up" \
    && apt-get clean \
    && rm -rvf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT ["dumb-init","--","/run-mattermost.sh"]
WORKDIR /mattermost
CMD ["mattermost"]
