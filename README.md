# k8s Mattermost

Mattomo image.

Based on https://github.com/mattermost/mattermost-docker

Depends on postgresql or mysql

Build with:

```
$ make build
```

May ship with OpenShift client:

```
$ make buildoc
```

Warning: LDAP integration requires enterprise. Not tested yet. SAML also
unavailable using their community edition. OIDC works with LemonLDAP.

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name               |    Description                     | Default                                                        |
| :----------------------------- | ---------------------------------- | -------------------------------------------------------------- |
|  `AUTH_METHOD`                 | Mattermost Users Auth              | `internal`, could be `lemon`, `ldap`, `oidc` or `internal`     |
|  `DB_DRIVER`                   | Mattermost Database Driver         | `mysql`                                                        |
|  `DB_NAME`                     | Mattermost Database Name           | `mattermost`                                                   |
|  `DB_HOST`                     | Mattermost Database Host           | `mysql`                                                        |
|  `DB_PASS`                     | Mattermost Database Password       | `secret`                                                       |
|  `DB_PORT`                     | Mattermost Database Port           | `5432` if `$DB_DRIVER=postgres`, `3306` otherwise              |
|  `DB_USER`                     | Mattermost Database Username       | `mattermost`                                                   |
|  `LANG`                        | Mattermost Admin Locale            | `en`                                                           |
|  `LLNG_SSO_DOMAIN`             | Mattermost OIDC SAML Address       | `auth.${OPENLDAP_DOMAIN}`                                      |
|  `LLNG_PROTO`                  | Mattermost OIDC Proto              | `http`                                                         |
|  `MM_ADMIN_PASSWORD`           | Mattermost Admin Password          | `MySecret-42`                                                  |
|  `MM_ADMIN_USERNAME`           | Mattermost Admin Username          | `mmadm`                                                        |
|  `MM_DOMAIN`                   | Mattermost Site Domain Name        | `mattermost.${OPENLDAP_DOMAIN}`                                |
|  `OIDC_CLIENT_ID`              | Mattermost OIDC Client ID          | `mattermost`                                                   |
|  `OIDC_LOCKOUT`                | Mattermost OIDC-Only Toggle        | undef                                                          |
|  `OIDC_SECRET`                 | Mattermost OIDC Secret             | `secret`                                                       |
|  `OPENLDAP_ADMIN_USER_FILTER`  | OpenLDAP Admin User Filter         | `(|(uid=admin0)(memberOf=cn=Admins,ou=groups,$OPENLDAP_BASE))` |
|  `OPENLDAP_BASE`               | OpenLDAP Base                      | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local`    |
|  `OPENLDAP_BIND_DN_RREFIX`     | OpenLDAP Bind DN Prefix            | `cn=mattermost,ou=services`                                    |
|  `OPENLDAP_BIND_PW`            | OpenLDAP Bind Password             | `secret`                                                       |
|  `OPENLDAP_DOMAIN`             | OpenLDAP Domain Name               | `demo.local`                                                   |
|  `OPENLDAP_HOST`               | OpenLDAP Backend Address           | undef                                                          |
|  `OPENLDAP_PORT`               | OpenLDAP Bind Port                 | `389` or `636` depending on `OPENLDAP_PROTO`                   |
|  `OPENLDAP_PROTO`              | OpenLDAP Proto                     | `ldap`                                                         |
|  `OPENLDAP_USERS_OBJECTCLASS`  | OpenLDAP Users ObjectClass         | `inetOrgPerson`                                                |
|  `PROMETHEUS_ENABLE`           | Mattermost Prometheus Exporter     | `false`                                                        |
|  `PUBLIC_PROTO`                | Mattermost Public Proto            | `http`                                                         |
|  `S3_ACCESS_KEY`               | S3 ObjectStore Access Key          | undef                                                          |
|  `S3_BUCKET`                   | S3 ObjectStore Bucket              | `mattermost`                                                   |
|  `S3_ENDPOINT`                 | S3 ObjectStore Endpoint            | `s3.amazonaws.com`                                             |
|  `S3_REGION`                   | S3 ObjectStore Region              | `us-east-1`                                                    |
|  `S3_SECRET_KEY`               | S3 ObjectStore Secret Key          | undef                                                          |
|  `S3_SIG_V2`                   | S3 ObjectStore Uses V2 Sigatures   | `false`                                                        |
|  `S3_SSL`                      | S3 ObjectStore Uses TLS            | `true`                                                         |
|  `SITE_NAME`                   | Mattermost Site Name               | `KubeChat`                                                     |
|  `SMTP_HOST`                   | Mattermost SMTP Relay              | undef                                                          |
|  `SMTP_NOREPLY`                | Mattermost SMTP Noreply            | `noreply@$OPENLDAP_DOMAIN`                                     |
|  `SMTP_PORT`                   | Mattermost SMTP Port               | `25`                                                           |
|  `STORAGE_DRIVER`              | Mattermost Storage Driver          | `local`                                                        |
|  `WS_PROTO`                    | Mattermost Websocket Proto         | `ws`                                                           |

You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point           | Description                 |
| :---------------------------- | --------------------------- |
|  `/mattermost/config`         | Mattermost Configuration    |
|  `/mattermost/data`           | Mattermost Data Directory   |
|  `/mattermost/logs`           | Mattermost Logs             |
|  `/mattermost/plugins`        | Mattermost Plugins          |
|  `/mattermost/client/plugins` | Mattermost Client Plugins   |
|  `/certs`                     | CA Certificates (optional)  |
